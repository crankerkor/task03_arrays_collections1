package com.oleksandr.generics;

public class Airdrome {
    public static void main(String[] args) {
        Ship<Droid> ship = new Ship<>();

        Droid C3PO = new TranslatorDroid("C3PO", 132, 13);
        Droid R2D2 = new MechanicDroid("R2D2");

        ship.add(C3PO);
        ship.add(R2D2);

        ((TranslatorDroid) C3PO).translate(C3PO);
    }
}
