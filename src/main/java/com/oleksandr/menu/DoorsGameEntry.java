package com.oleksandr.menu;

import com.oleksandr.arrays.ArrayGame;

public class DoorsGameEntry extends MenuEntry {
    ArrayGame game;

    public DoorsGameEntry() {
        game = new ArrayGame();
    }

    public void run() {
        game.fillDoors();
    }
}
