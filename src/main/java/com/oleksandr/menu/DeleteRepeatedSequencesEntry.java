package com.oleksandr.menu;

import com.oleksandr.arrays.ArrayWorker;

public class DeleteRepeatedSequencesEntry extends ArrayTasksEntry {
    private ArrayWorker arrayWorker;
    private int[] array;

    public DeleteRepeatedSequencesEntry() {
        arrayWorker = new ArrayWorker();
        this.setExplanation("Delete repeated sequences from array");
    }

    public void run() {
        array = getArray();

        printArray(array);

        int[] result = arrayWorker.deleteRepeatedSequences(array);
        printArray(result);

    }
}
