package com.oleksandr.menu;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Menu {
    List<MenuEntry> entries;

    public Menu() {
        entries = new ArrayList<>();
    }

    public final void initializeEntries() {
        entries.add(new ExitEntry());
        entries.add(new ArrayTasksEntry());
        entries.add(new DoorsGameEntry());
    }

    public final void printMenu() {
        MenuEntry chosenOption;
        int chosenOptionNumber;

        do {
            System.out.println("\nChoose, what you want to do:");

            for (int i = 0; i < entries.size(); i++) {
                System.out.println(i + ") " + entries.get(i).getExplanation());
            }

            chosenOptionNumber = chooseOption(entries.size());

            chosenOption = entries.get(chosenOptionNumber);

            chosenOption.run();

        } while (!(chosenOption instanceof ExitEntry));
    }

    private static int chooseOption(final int size) {
        int chosenOptionNumber;
        Scanner scanner = new Scanner(System.in, "UTF-8");

        do {
            System.out.print("Please enter a valid option: ");

            while (!scanner.hasNextInt()) {
                scanner.next();
                System.out.print("Please enter a valid option: ");
            }

            chosenOptionNumber = scanner.nextInt();
        } while (chosenOptionNumber >= size);

        return chosenOptionNumber;
    }
}
