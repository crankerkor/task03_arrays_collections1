package com.oleksandr.menu;

public abstract class MenuEntry {
    private String explanation;

    abstract void run();

    final String getExplanation() {
        return explanation;
    }

    final void setExplanation(final String exp) {
        this.explanation = exp;
    }
}
