package com.oleksandr.menu;

public class ExitEntry extends MenuEntry {

    ExitEntry() {
        this.setExplanation("Exit the program");
    }

    @Override
    void run() {
        System.out.print("Goodbye");
        System.exit(0);
    }
}
