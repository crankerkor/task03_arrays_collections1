package com.oleksandr.menu;

import com.oleksandr.arrays.ArrayWorker;

public class SymmetricDifferenceEntry extends ArrayTasksEntry {
    private ArrayWorker arrayWorker;
    private int[] array1;
    private int[] array2;

    public SymmetricDifferenceEntry() {
        arrayWorker = new ArrayWorker();
        this.setExplanation("Symmetric difference of 2 arrays");
    }

    public void run() {
        array1 = getArray();
        array2 = getArray();

        printArray(array1);
        printArray(array2);

        int[] result = arrayWorker.symmetricDifference(array1, array2);
        printArray(result);

    }

}
