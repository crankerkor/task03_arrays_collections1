package com.oleksandr.menu;

import com.oleksandr.arrays.ArrayWorker;

public class IntersectionEntry extends ArrayTasksEntry {
    private ArrayWorker arrayWorker;
    int[] array1;
    int[] array2;

    public IntersectionEntry() {
        arrayWorker = new ArrayWorker();
        this.setExplanation("Intersection of 2 arrays");
    }

    public void run() {
        array1 = getArray();
        array2 = getArray();

        printArray(array1);
        printArray(array2);

        int[] result = arrayWorker.intersection(array1, array2);

        printArray(result);

    }


}
