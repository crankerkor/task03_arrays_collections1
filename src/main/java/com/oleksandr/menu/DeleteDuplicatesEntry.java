package com.oleksandr.menu;

import com.oleksandr.arrays.ArrayWorker;

public class DeleteDuplicatesEntry extends ArrayTasksEntry {
    private ArrayWorker arrayWorker;
    private int[] array;

    public DeleteDuplicatesEntry() {
        arrayWorker = new ArrayWorker();
        this.setExplanation("Delete duplicates from array");
    }

    public void run() {
        array = getArray();

        printArray(array);

        int[] result = arrayWorker.deleteDuplicates(array);
        printArray(result);

    }
}
