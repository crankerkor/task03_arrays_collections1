package com.oleksandr.menu;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

public class ArrayTasksEntry extends MenuEntry {
    private List<MenuEntry> subEntries;

    public ArrayTasksEntry() {
        subEntries = new ArrayList<>();
        this.setExplanation("Array tasks");
    }

    private void initializeEntries() {
        subEntries.add(new IntersectionEntry());
        subEntries.add(new SymmetricDifferenceEntry());
        subEntries.add(new DeleteDuplicatesEntry());
        subEntries.add(new DeleteRepeatedSequencesEntry());
    }

    public void run() {
        initializeEntries();

        MenuEntry chosenOption;
        int chosenOptionNumber;

            System.out.println("\nChoose, what you want to do:");

            for (int i = 0; i < subEntries.size(); i++) {
                System.out.println(i + ") " + subEntries.get(i).getExplanation());
            }

            chosenOptionNumber = chooseOption(subEntries.size());

            chosenOption = subEntries.get(chosenOptionNumber);
            chosenOption.run();

        subEntries = new ArrayList<>();
    }

    private static int chooseOption(final int size) {
        int chosenOptionNumber;
        Scanner scanner = new Scanner(System.in, "UTF-8");

        do {
            System.out.print("Please enter a valid option: ");

            while (!scanner.hasNextInt()) {
                scanner.next();
                System.out.print("Please enter a valid option: ");
            }

            chosenOptionNumber = scanner.nextInt();
        } while (chosenOptionNumber >= size);

        return chosenOptionNumber;
    }

    protected void printArray(int[] arr) {
        for(int elem : arr) {
            System.out.print(elem + " ");
        }
        System.out.println();
    }

    protected int[] fillArray(int[] arr) {
        Scanner scanner = new Scanner(System.in, "utf-8");

        for (int i = 0; i < arr.length; i++) {
            System.out.print("Enter " + i + " element");

            while (!scanner.hasNextInt()) {
                scanner.next();
                System.out.print("Please enter an integer: ");
            }
            arr[i] = scanner.nextInt();
        }

        return arr;
    }

    protected int[] randomlyFillArray(int[] arr) {
        Random rand = new Random();

        for (int i = 0; i < arr.length; i++) {
            arr[i] = rand.nextInt(10);
        }

        return arr;
    }

    protected int[] getArray() {
            System.out.println("Enter the length of an array");
            int arrayLength = chooseOptionWithBounds(0, Integer.MAX_VALUE);
            int[] array = new int[arrayLength];

            System.out.println("How would you like to fill aray? " +
                    ("\n1) Manually") + "\n2) Randomly");

            int chosenOptionNumber = chooseOptionWithBounds(0, 2);

            switch(chosenOptionNumber) {
                case 1:
                    System.out.println("Fill the array");
                    array = fillArray(array);
                    break;

                case 2:
                    array = randomlyFillArray(array);
                    break;
                default:
                    break;
            }

            return array;
        }

    private int chooseOptionWithBounds(int leftBound, int rightBound) {
            int chosenOptionNumber;

            Scanner scanner = new Scanner(System.in, "utf-8");

            do {
                System.out.print("Please enter a valid option: ");

                while (!scanner.hasNextInt()) {
                    scanner.next();
                    System.out.print("Please enter a valid option: ");
                }

                chosenOptionNumber = scanner.nextInt();
            } while (chosenOptionNumber > rightBound || chosenOptionNumber < leftBound);

            return chosenOptionNumber;
    }

}
