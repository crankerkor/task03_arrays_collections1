package com.oleksandr.arrays;

import com.oleksandr.menu.Menu;

public class ArrayFun {
    public static void main(String[] args) {
        int[] array1 = {1,1,2,2,2,2,3,4,4,4,5,6};
        int[] array2 = {2,2,3,4,4,5,7};

        Menu menu = new Menu();
        menu.initializeEntries();
        menu.printMenu();

        ArrayGame game = new ArrayGame();
        game.fillDoors();
    }
}
